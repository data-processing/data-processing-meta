<!-- TABLE OF CONTENTS -->
## Table of Contents

<details open="open">
   <ul>
      <li><a href="#sequence-diagram">Sequence Diagram</a></li>
      <li><a href="#high-level-diagram">High Level Diagram</a></li>
      <li><a href="#processor-eer-diagram">Processor EER Diagram</a></li>
      <li>
         <a href="#files-and-directories-structure">Files and Directories Structure</a>
         <ul>
            <li><a href="#project-structure">Project Structure</a></li>
            <li><a href="#packages">Packages</a></li>
         </ul>
      </li>
   </ul>
</details>

### Sequence Diagram

[![Sequence Diagram](../images/DP-Sequence-Diagram.png)](../images/DP-Sequence-Diagram.png)

### High Level Diagram

### Local Environment

[![High Level Diagram For Local](../images/DP-Concept-Local.png)](../images/DP-Concept-Local.png)

### Production Environment

[![High Level Diagram For Production](../images/DP-Concept-Prod.png)](../images/DP-Concept-Prod.png)

## Processor EER Diagram

[![Processor EER Diagram](../images/DP-Processor-ERR.png)](../images/DP-Processor-ERR.png)

## Files and Directories Structure

The project (a.k.a. project directory) has a particular directory structure. A representative project is shown below:

### Analyzer Project Structure

```text
.
├── src
│   └── main
│       └── java
│           ├── com.mycompany.dp.analyzer
│           │ 
│           ├──com.mycompany.dp.analyzer.config
│           ├──com.mycompany.dp.analyzer.config.properties
│           │  
│           ├──com.mycompany.dp.analyzer.dto
|           |
│           ├──com.mycompany.dp.analyzer.exception
|           |
│           ├──com.mycompany.dp.analyzer.handler
|           |
│           ├──com.mycompany.dp.analyzer.jms
|           |
│           ├──com.mycompany.dp.analyzer.manager
|           |
│           ├──com.mycompany.dp.analyzer.model
|           |
│           ├──com.mycompany.dp.analyzer.repository
|           |
│           ├──com.mycompany.dp.analyzer.route
|           |
│           ├──com.mycompany.dp.analyzer.service
|           |
│           ├──com.mycompany.dp.analyzer.validator
|           |
│           ├──com.mycompany.dp.analyzer.webfilter
|           |
|           └──com.mycompany.dp.analyzer.AnalyzerApplication.java
├── src
│   └── main
│       └── resources
│           ├── application-dev.yml
│           ├── application-prod.yml
│           ├── application-dev.yml
│           └── logback-spring.xml
├── pom.xml
├── Dockerfile
└── README.md
```
### Packages

* 	`config` - app configurations;
*   `dto` - to hold out DTOs;
* 	`exception` - to hold custom exception handling;
*   `handler` - to hold handlers from route;
*   `jms` - contains JMS managers as well related POJOs;
* 	`model` - to hold our entities;
* 	`repository` - to communicate with the database;
* 	`route` - to hold app endpoints to listen to the client;
* 	`service` - to hold business logic;
* 	`validator` - to validate client requests;
* 	`webfilter` - to hold web filters;

* 	`resources/` - Contains all the static resources, templates and property files.
* 	`resources/application.yaml` - It contains application-wide properties. Spring reads the properties defined in this file to configure your application. You can define server’s default port, server’s context path, database URLs etc, in this file.
*
* 	`pom.xml` - contains all the project dependencies
*
* 	`Dockerfile` - defines docker image

All the other components has similar structure so they are not defined separately.
